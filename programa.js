function iniciarCalculadora() 
{
    let boton1;
    let boton2;
    let formulario;
    let i;
    let j;
    let salto1;
    let salto2;

    formulario = document.getElementById("tableroCalculador");

    for (j = 1; j < 3; j++) {
        boton1 = document.createElement("input");

        //Asignación de atributos
        boton1.setAttribute("type", "text");
        boton1.setAttribute("id", "boton" + j);
        boton1.setAttribute("class", "boton");
        formulario.appendChild(boton1);
        if (j % 2 == 0) {
            salto1 = document.createElement("br");
            formulario.appendChild(salto1);
        }
    }

    for (i = 1; i < 8; i++) {
        boton2 = document.createElement("input");

        //Asignación de atributos
        boton2.setAttribute("type", "button");
        boton2.setAttribute("id", "boton" + (i + 2));
        boton2.setAttribute("class", "boton2");
        formulario.appendChild(boton2);
        if (i % 3 == 0) {
            salto2 = document.createElement("br");
            formulario.appendChild(salto2);
        }
    }
    document.getElementById("boton3").setAttribute("onclick", "miCalculadorAritmetica.sumar();");
    document.getElementById("boton4").setAttribute("onclick", "miCalculadorAritmetica.restar();");
    document.getElementById("boton5").setAttribute("onclick", "miCalculadorAritmetica.modular();");
    document.getElementById("boton6").setAttribute("onclick", "miCalculadora.potencia();");
    document.getElementById("boton7").setAttribute("onclick", "miCalculadora.logaritmox();");
    document.getElementById("boton8").setAttribute("onclick", "miCalculadora.logaritmoy();");
    // asignacion de texto a los botones

    document.getElementById("boton3").value = "+";
    document.getElementById("boton4").value = "-";
    document.getElementById("boton5").value = "%";
    document.getElementById("boton6").value = "x^y";
    document.getElementById("boton7").value = "Log x";
    document.getElementById("boton8").value = "Log y";
}


class CalculadorAritmetica
{
    //Atributos
    numero1;
    numero2;

    //Método
    sumar()
    {
        let suma;
        suma = parseInt(document.getElementById('boton1').value) + parseInt(document.getElementById('boton2').value);
        document.getElementById("boton9").value = suma;
        return suma;
    }
    restar()
    {
        let resta;
        resta = parseInt(document.getElementById('boton1').value) - parseInt(document.getElementById('boton2').value);
        document.getElementById("boton9").value = resta;
        return resta;
    }
    modular()
    {
        let modulo;
        modulo = parseInt(document.getElementById('boton1').value) % parseInt(document.getElementById('boton2').value);
        document.getElementById("boton9").value = modulo;
        return modulo;
    }
}


let miCalculadorAritmetica=new CalculadorAritmetica();



class Calculador
{
    //Atributos
    numero1;
    numero2;

    //Metodo
    potencia()
    {
        let elevado;
        elevado = Math.pow(parseInt(document.getElementById('boton1').value), parseInt(document.getElementById('boton2').value));
        document.getElementById("boton9").value = elevado;
        return elevado;
    }
    logaritmox()
    {
        let logx;
        logx = Math.log10(parseInt(document.getElementById('boton1').value));
        document.getElementById("boton9").value = logx;
        return logx;
    }
    logaritmoy()
    {
        let logy;
        logy = Math.log10(parseInt(document.getElementById('boton1').value));
        document.getElementById("boton9").value = logy;
        return logy;
    }
}

let miCalculadora=new Calculador();


